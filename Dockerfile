FROM python:3.10-alpine
RUN apk add git
WORKDIR /app
COPY .git ./.git
COPY bots ./bots
COPY mechascrypt ./mechascrypt
COPY scripts ./scripts
RUN python scripts/megalock.py
RUN pip install pipenv
RUN pipenv install --system --ignore-pipfile --deploy
CMD ["python", "scripts/bots.py"]