# mechamancers

The mechamancers production root directory.

## I want to develop. Where do I start?

Firstly, thank you for wanting to develop. you fuck.

This repository isn't designed for development, so you don't need to clone it for development. 

Clone the module directory of the bot that you want to work on. For example, if you wanted to work on ``wizx``, you would clone [``wizx``](https://gitlab.com/8o-collective/mechamancers/wizx). Then, you can commit changes and push straight to ``development``, or if your implementation will take a while to develop, checkout a new feature branch and merge it into ``development`` later.

NOTE: you may not have certain API files containing tokens neccesary to run the bot - mainly, the ``auth.json`` file. These are not included in the development repository for security reasons, but can be requested from admins or other developers who have it.

Every module uses [discord.py](https://discordpy.readthedocs.io/en/stable/) for development. To test it locally: 
1. Make a ``venv`` with ``python3 -m venv venv``.
2. Activate it with ``source ./venv/bin/activate``. 
3. Install the requirements with ``pip install -r requirements.txt``.
4. Run ``python start.py``.

If you change a file, you will have to re-run the bot or your changes will not update.

## I want to push to production. What should I do?

Well well well, you think your changes are good enough to push to production. Tsk.

Unless you're making an entirely new bot (discussed below), you don't need to clone this repository to push to production.

After committing and pushing to the ``development`` branch of the bot of your choice, make a commit with message ``vX.X.X`` that only changes the ``__version__`` variable in the bot's ``__init.py__``. Then, merge ``development`` into ``production``.

NOTE: Version numbers go [major].[minor].[patch]

To see any production changes on the server, the Docker image on Federado will have to be rebuilt. This can be done by SSHing into Federado, running ``git submodule update --init --recursive`` and ``docker-compose down && docker-compose up -d``.

NOTE: You will have to copy token files into Federado, as specified in a note in the previous section.

## I want to make a new bot or add an existing one to production. How do I do that?

To make a new bot, start by making a new bot repository.

After the repository is created, feel free to copy the code from an existing module to get started. It is reccommended to use [``wizx``](https://gitlab.com/8o-collective/mechamancers/wizx/) as that bot is generally a good example of how modules should be structured.

When a new module is created in any project, it must be specified in the parent module identifier in order to be pushed to production. This can be done by cloning the identifier, running ``git submodule add https://link/to/module``, making a commit message ``Added submodule [name of the module]`` and pushing.

Like any other production change, the Docker image on Federado will have to be rebuilt before changes are seen on [8oc.org](http://8oc.org). This process is described in the above section.