#!/bin/sh

NAMES=$(awk 'BEGIN{for(v in ENVIRON) print v}' | grep ^AUTH)

for name in $NAMES
do
	eval VALUE=\$$name
	FINAL="${FINAL}${name}: $(echo $VALUE | tr -d '\n' | base64 | tr -d '\n')\n"
done

printf "$FINAL"