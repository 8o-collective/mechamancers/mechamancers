import asyncio
import os
import sys

bots = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../bots')

async def run(bot):
	with open(os.path.join(bots, bot, "auth.json"), "w+") as f:
		f.write(os.environ[f"AUTH_{bot.upper()}"])

	process = await asyncio.create_subprocess_exec(
        sys.executable, "-m", bot,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
		env={"PYTHONPATH": os.path.join(bots, bot)})

	stdout, stderr = await process.communicate()

	print(f"[{bot} exited with {process.returncode}]")
	if stdout:
		print(f'[stdout]\n{stdout.decode()}')
	if stderr:
		print(f'[stderr]\n{stderr.decode()}')

async def main():
	await asyncio.gather(*[run(bot) for bot in os.listdir(bots)])

asyncio.run(main())